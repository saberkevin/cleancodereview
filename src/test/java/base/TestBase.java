package base;

import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.PropertyConfigurator;
import org.junit.Assert;
import org.testng.annotations.BeforeClass;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class TestBase {
	public static RequestSpecification httpRequest;
	public static Response response;
	public String personalID = "4";
	
	public Logger logger;
	
	@BeforeClass
	public void setup()
	{
		logger = Logger.getLogger("restAPI");
		PropertyConfigurator.configure("Log4j.properties");
		//logger.setLevel(Level.DEBUG);
	}
	
	public void checkElementAgeUnder(int eage)
	{
		JsonPath jsonPath = response.jsonPath();
		int age = jsonPath.get("age");
		logger.info(String.valueOf(age));
		
		if(age >= eage)
		{
			logger.warning("Age must be under " + eage);
		}
		
		Assert.assertTrue(age < eage);	
	}
	
	public void checkElementURLValid()
	{
		JsonPath jsonPath = response.jsonPath();
		String ownWebsite = jsonPath.get("ownWebsite");
		logger.info(ownWebsite);
		
		String regex = "^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$";
		 
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(ownWebsite);
		
		boolean isValid = matcher.matches();
		
		Assert.assertTrue(isValid);	
	}
	
	public void checkElementAgeValid()
	{
		JsonPath jsonPath = response.jsonPath();
		int age = jsonPath.get("age");
		logger.info(String.valueOf(age));
		
		String regex = "^[0-9][0-9]+$";
		 
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(String.valueOf(age));
		
		boolean isValid = matcher.matches();
		
		Assert.assertTrue(isValid);	
	}
	
	public void checkElementPersonalCardIdValid()
	{
		JsonPath jsonPath = response.jsonPath();
		String perCardId = jsonPath.get("perCardId");
		logger.info(perCardId);
		
		String regex = "^(P)[0-9][0-9][0-9]+$";
		 
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(perCardId);
		
		boolean isValid = matcher.matches();
		
		Assert.assertTrue(isValid);	
	}
	
	public void checkElementEmpty()
	{
		JsonPath jsonPath = response.jsonPath();
		logger.info(jsonPath.get("id").toString());
		logger.info(jsonPath.get("name").toString());
		logger.info(jsonPath.get("age").toString());
		logger.info(jsonPath.get("email").toString());
		
		Assert.assertNotEquals(jsonPath.get("id"), "");
		Assert.assertNotEquals(jsonPath.get("name"), "");
		Assert.assertNotEquals(jsonPath.get("age"), 0);
		Assert.assertNotEquals(jsonPath.get("email"), "");
	}
	
	public void checkElementEmailValid()
	{
		JsonPath jsonPath = response.jsonPath();
		String email = jsonPath.get("email");
		logger.info(email);
		
		String regex = "^(.+)@(.+)$";
		 
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(email);
		
		boolean isValid = matcher.matches();
		
		Assert.assertTrue(isValid);	
	}
	
	public void checkResponseBodyContain(String sName, int sAge, boolean sActive, String sEmail, String sOwnWebsite, String sPerCardId)
	{
		logger.info("***** Check Response Body (Contain) *****");
		
		String responseBody = response.getBody().asString();
		logger.info("Response Body = " + responseBody);
		Assert.assertTrue(responseBody.contains(sName));
		Assert.assertTrue(responseBody.contains(String.valueOf(sAge)));
		Assert.assertTrue(responseBody.contains(Boolean.toString(sActive)));
		Assert.assertTrue(responseBody.contains(sEmail));
		Assert.assertTrue(responseBody.contains(sOwnWebsite));
		Assert.assertTrue(responseBody.contains(sPerCardId));
	}
	
	public void checkResponseBodyNotEmpty()
	{
		logger.info("***** Check Response Body (Not Empty) *****");
		
		String responseBody = response.getBody().asString();
		logger.info("Response Body = " + responseBody);
		Assert.assertTrue(responseBody != null && !responseBody.equals("") && !responseBody.equals("[]"));
	}
	
	public void checkResponseBodySingleItemNotEmpty()
	{
		logger.info("***** Check Response Body (Not Empty and Contain ID) *****");
		
		String responseBody = response.getBody().asString();
		logger.info("Response Body = " + responseBody);
		Assert.assertTrue(responseBody != null && !responseBody.equals("") && !responseBody.equals("[]"));
		Assert.assertTrue(responseBody.contains(personalID));
	}
	
	public void checkStatusCode(String sc)
	{
		logger.info("***** Check Status Code *****");
		
		int statusCode = response.getStatusCode();
		logger.info("Status Code = " + statusCode);
		Assert.assertEquals(statusCode, Integer.parseInt(sc));	
	}
	
	public void checkResponseTime(String rt)
	{
		logger.info("***** Check Response Time *****");
		
		long responseTime = response.getTime();
		logger.info("Response Time = " + responseTime);
		Assert.assertTrue(responseTime<Long.parseLong(rt));
	}
	
	public void checkStatusLine(String sl)
	{
		logger.info("***** Check Status Line *****");
		
		String statusLine = response.getStatusLine();
		logger.info("Status Line = " + statusLine);
		Assert.assertEquals(statusLine, sl);
	}
	
	public void checkContentType(String ct)
	{
		logger.info("***** Check Content Type *****");
		
		String contentType = response.header("Content-Type");
		logger.info("Content Type = " + contentType);
		Assert.assertEquals(contentType, ct);
	}
	
	public void checkServerType(String st)
	{
		logger.info("***** Check Server Type *****");
		
		String serverType = response.header("Server");
		logger.info("Server Type = " + serverType);
		Assert.assertEquals(serverType, st);
	}
	
	public void checkContentEncoding(String ce)
	{
		logger.info("***** Check Content Encoding *****");
		
		String contentEncoding = response.header("Content-Encoding");
		logger.info("Content Encoding = " + contentEncoding);
		Assert.assertEquals(contentEncoding, ce);
	}
	
	public void checkContentLength(String cl)
	{
		logger.info("***** Check Content Length *****");
		
		String contentLength = response.header("Content-Length");
		logger.info("Content Length = " + contentLength);
		
		int contLeng = 0;
		if(contentLength != null)
		{
			contLeng = Integer.parseInt(contentLength);
		}
		
		int contLengParam = Integer.parseInt(cl);
		
		if(contLeng < contLengParam)
		{
			logger.warning("Content Length is less than " + contLengParam);
		}
		
		Assert.assertTrue(contLeng>contLengParam || contLeng == 0);	
	}
	
	public void checkCookies(String ck)
	{
		logger.info("***** Check Cookies *****");
		String cookie = response.getCookie(ck);
		logger.info("Cookie = " + cookie);
	}
	
	public void tearDown(String message)
	{
		logger.info("***** " + message + " *****");
	}
}
