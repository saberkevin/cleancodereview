package testCases;

import org.json.simple.JSONObject;
import org.junit.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import base.TestBase;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import utilities.RestUtils;

public class TC_004_Put_Personal_Data extends TestBase {
	String personalCardId =  RestUtils.personalCardId();
	String personalName = RestUtils.personalName();
	int personalAge = Integer.parseInt(RestUtils.personalAge());
	String personalEmail = RestUtils.personalEmail();
	String personalOwnWebsite = RestUtils.personalOwnWebsite();
	boolean personalActive = Boolean.parseBoolean(RestUtils.personalActive());

	@SuppressWarnings("unchecked")
	@BeforeClass
	void updateSiswa() throws InterruptedException
	{
		logger.info("***** " + this.getClass().getSimpleName() + " *****");
		RestAssured.baseURI = "https://5e9fa90c11b078001679ca00.mockapi.io";
		httpRequest = RestAssured.given();
		
		JSONObject requestParams = new JSONObject();
		
		requestParams.put("perCardId", personalCardId);
		requestParams.put("name", personalName);
		requestParams.put("age", personalAge);
		requestParams.put("email", personalEmail);
		requestParams.put("ownWebsite", personalOwnWebsite);
		requestParams.put("active", personalActive);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(requestParams.toJSONString());
		
		response = httpRequest.request(Method.PUT, "/personal/" + personalID);
		
		Thread.sleep(5);
	}
	
	@Test
	void assertResponseBodyContain()
	{
		checkResponseBodyContain(personalName, personalAge, personalActive, personalEmail, personalOwnWebsite, personalCardId);
	}
	
	@Test
	@Parameters("statusCode")
	void assertStatusCode(String sc)
	{
		checkStatusCode(sc);	
	}
	
	@Test
	@Parameters("responseTime")
	void assertResponseTime(String rt)
	{
		checkResponseTime(rt);
	}
	
	@Test
	@Parameters("statusLine")
	void assertStatusLine(String sl)
	{
		checkStatusLine(sl);
	}
	
	@Test
	@Parameters("contentType")
	void assertContentType(String ct)
	{
		checkContentType(ct);
	}
	
	@Test
	@Parameters("serverType")
	void assertServerType(String st)
	{
		checkServerType(st);
	}
	
	@Test
	@Parameters("contentEncodingAlt")
	void assertContentEncoding(String ce)
	{
		checkContentEncoding(ce);
	}
	
	@AfterClass
	void end()
	{
		tearDown("Finished " + this.getClass().getSimpleName());
	}
}
