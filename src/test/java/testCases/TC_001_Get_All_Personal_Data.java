package testCases;

import org.junit.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import base.TestBase;
import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_001_Get_All_Personal_Data extends TestBase{
	
	@BeforeClass
	void getAllPersonalData() throws InterruptedException
	{
		logger.info("***** Started " + this.getClass().getSimpleName() + " *****");
		RestAssured.baseURI = "https://5e9fa90c11b078001679ca00.mockapi.io";
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, "/personal");
		
		Thread.sleep(5);
		
	}
	
	@Test
	void assertEmptyResponseBody()
	{
		checkResponseBodyNotEmpty();
	}
	
	@Test
	@Parameters("statusCode")
	void assertStatusCode(String sc)
	{
		checkStatusCode(sc);	
	}
	
	@Test
	@Parameters("responseTime")
	void assertResponseTime(String rt)
	{
		checkResponseTime(rt);
	}
	
	@Test
	@Parameters("statusLine")
	void assertStatusLine(String sl)
	{
		checkStatusLine(sl);
	}
	
	@Test
	@Parameters("contentType")
	void assertContentType(String ct)
	{
		checkContentType(ct);
	}
	
	@Test
	@Parameters("serverType")
	void assertServerType(String st)
	{
		checkServerType(st);
	}
	
	@Test
	@Parameters("contentLength")
	void assertContentLength(String cl)
	{
		checkContentLength(cl);
	}
	
	@Test
	@Parameters("contentEncoding")
	void assertContentEncoding(String ce)
	{
		checkContentEncoding(ce);
	}
	
	@Test
	@Parameters("cookies")
	void chkCookies(String ck)
	{
		checkCookies(ck);
	}
	
	@AfterClass
	void end()
	{
		tearDown("Finished " + this.getClass().getSimpleName());
	}
}
