package testCases;

import org.junit.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import base.TestBase;
import io.restassured.RestAssured;
import io.restassured.http.Method;

public class TC_002_Get_Single_Personal_Data extends TestBase{
	
	@BeforeClass
	void getSinglePersonalData() throws InterruptedException
	{
		logger.info("***** Started " + this.getClass().getSimpleName() + " *****");
		RestAssured.baseURI = "https://5e9fa90c11b078001679ca00.mockapi.io";
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, "/personal/" + personalID);
		
		Thread.sleep(5);
	}
	
	@Test(priority = 10)
	@Parameters("ageUnder")
	void assertElementAgeUnder(String eage)
	{
		checkElementAgeUnder(Integer.parseInt(eage));
	}
	
	@Test(priority = 12)
	void assertElementOwnWebsiteValid()
	{
		checkElementURLValid();
	}
	
	@Test(priority = 9)
	void assertElementAgeValid()
	{
		checkElementAgeValid();
	}
	
	@Test(priority = 8)
	void assertElementPersonalCardIdValid()
	{
		checkElementPersonalCardIdValid();
	}
	
	@Test(priority = 0)
	void assertEmptyElement()
	{
		checkElementEmpty();
	}
	
	@Test(priority = 11)
	void assertElementEmailValid()
	{
		checkElementEmailValid();
	}
	
	@Test(priority = 1)
	void assertEmptyResponseBody()
	{
		checkResponseBodySingleItemNotEmpty();
	}
	
	@Test(priority = 2)
	@Parameters("statusCode")
	void assertStatusCode(String sc)
	{
		checkStatusCode(sc);	
	}
	
	@Test(priority = 3)
	@Parameters("responseTime")
	void assertResponseTime(String rt)
	{
		checkResponseTime(rt);
	}
	
	@Test(priority = 4)
	@Parameters("statusLine")
	void assertStatusLine(String sl)
	{
		checkStatusLine(sl);
	}
	
	@Test(priority = 5)
	@Parameters("contentType")
	void assertContentType(String ct)
	{
		checkContentType(ct);
	}
	
	@Test(priority = 6)
	@Parameters("serverType")
	void assertServerType(String st)
	{
		checkServerType(st);
	}
	
	@Test(priority = 7)
	@Parameters("contentLength")
	void assertContentLength(String cl)
	{
		checkContentLength(cl);
	}
	
	@AfterClass
	void end()
	{
		tearDown("Finished " + this.getClass().getSimpleName());
	}
}
