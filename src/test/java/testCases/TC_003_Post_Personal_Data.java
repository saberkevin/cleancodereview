package testCases;

import java.io.IOException;

import org.json.simple.JSONObject;
import org.junit.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import base.TestBase;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import utilities.ExcelUtil;

public class TC_003_Post_Personal_Data extends TestBase {
	
	@BeforeClass
	void init()
	{
		RestAssured.baseURI = "https://5e9fa90c11b078001679ca00.mockapi.io";
		httpRequest = RestAssured.given();
	}
	
	@DataProvider(name = "personalData")
	String[][] getExcelData() throws IOException
	{
		String path = "/Users/kevinwinarko/eclipse-workspace/Exam5/src/test/java/testCases/InsertData.xlsx";
		
		int rowCount = ExcelUtil.getRowCount(path, "Sheet1");
		int colCount = ExcelUtil.getCellCount(path, "Sheet1",1);
		
		String personalData[][] = new String[rowCount][colCount];
		
		for(int i=1; i<=rowCount; i++)
		{
			for(int j=0; j<colCount; j++)
			{
				personalData[i-1][j] = ExcelUtil.getCellData(path, "Sheet1", i, j);
			}
		}
		
		return personalData;
	}
	
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "personalData", priority = 0)
	void CreatePersonalData(String epercardid, String ename, String eage, String eemail, String eownwebsite, String eactive) throws InterruptedException
	{
		logger.info("***** Started " + this.getClass().getSimpleName() + " *****");
		JSONObject requestParams = new JSONObject();
		
		requestParams.put("perCardID", epercardid);
		requestParams.put("name", ename);
		requestParams.put("age", Integer.parseInt(eage));
		requestParams.put("email", eemail);
		requestParams.put("ownWebsite", eownwebsite);
		requestParams.put("active", Boolean.parseBoolean(eactive));
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(requestParams.toJSONString());
		
		response = httpRequest.request(Method.POST, "/personal");
		
		Thread.sleep(5);
	}
	
	@Test
	void assertEmptyResponseBody()
	{
		checkResponseBodyNotEmpty();
	}
	
	@Test
	@Parameters("statusCodePost")
	void assertStatusCode(String sc)
	{
		checkStatusCode(sc);	
	}
	
	@Test
	@Parameters("responseTime")
	void assertResponseTime(String rt)
	{
		checkResponseTime(rt);
	}
	
	@Test
	@Parameters("statusLinePost")
	void assertStatusLine(String sl)
	{
		checkStatusLine(sl);
	}
	
	@Test
	@Parameters("contentType")
	void assertContentType(String ct)
	{
		checkContentType(ct);
	}
	
	@Test
	@Parameters("serverType")
	void assertServerType(String st)
	{
		checkServerType(st);
	}
	
	@Test
	@Parameters("contentEncodingAlt")
	void assertContentEncoding(String ce)
	{
		checkContentEncoding(ce);
	}
	
	@AfterClass
	void end()
	{
		tearDown("Finished " + this.getClass().getSimpleName());
	}
}
