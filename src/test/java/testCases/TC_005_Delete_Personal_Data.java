package testCases;

import org.junit.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import base.TestBase;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;

public class TC_005_Delete_Personal_Data extends TestBase{
	@BeforeClass
	void deletePersonalData() throws InterruptedException
	{
		logger.info("***** Started " + this.getClass().getSimpleName() + " *****");
		RestAssured.baseURI = "https://5e9fa90c11b078001679ca00.mockapi.io";
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, "/personal");
		
		JsonPath jsonPathEvaluator = response.jsonPath();
		
		String perID = jsonPathEvaluator.get("[0].id");
		response = httpRequest.request(Method.DELETE, "/personal/" + perID);
		
		Thread.sleep(5);
	}
	
	@Test
	void assertResponseBodyEmpty()
	{
		checkResponseBodyNotEmpty();
	}
	
	@Test
	@Parameters("statusCode")
	void assertStatusCode(String sc)
	{
		checkStatusCode(sc);	
	}
	
	@Test
	@Parameters("responseTime")
	void assertResponseTime(String rt)
	{
		checkResponseTime(rt);
	}
	
	@Test
	@Parameters("statusLine")
	void assertStatusLine(String sl)
	{
		checkStatusLine(sl);
	}
	
	@Test
	@Parameters("contentType")
	void assertContentType(String ct)
	{
		checkContentType(ct);
	}
	
	@Test
	@Parameters("serverType")
	void assertServerType(String st)
	{
		checkServerType(st);
	}
	
	@Test
	@Parameters("contentEncodingAlt")
	void assertContentEncoding(String ce)
	{
		checkContentEncoding(ce);
	}
	
	@AfterClass
	void end()
	{
		tearDown("Finished " + this.getClass().getSimpleName());
	}
}
